import {Middleware, NextMiddlewareFunction, Request, RequestState, Response} from '@dkx/http-server';
import {ResponseOptions} from './config';


export function responseMiddleware(options: ResponseOptions): Middleware
{
	return (req: Request, res: Response, next: NextMiddlewareFunction, state: RequestState): Promise<Response> =>
	{
		if (typeof options.onRequest !== 'undefined') {
			options.onRequest(req, state);
		}

		const statusCodeExist = typeof options.statusCode !== 'undefined';

		if (statusCodeExist || typeof options.statusMessage !== 'undefined') {
			res = res.withStatus(statusCodeExist ? options.statusCode : 200, options.statusMessage);
		}

		if (typeof options.headers !== 'undefined') {
			for (let headerName in options.headers) {
				if (options.headers.hasOwnProperty(headerName)) {
					res = res.withHeader(headerName, options.headers[headerName]);
				}
			}
		}

		if (typeof options.body !== 'undefined') {
			res.write(options.body);
		}

		if (typeof options.timeout === 'undefined') {
			return next(res);
		}

		return new Promise((resolve) => {
			setTimeout(async () => {
				resolve(await next(res));
			}, options.timeout);
		});
	};
}
