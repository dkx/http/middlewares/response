import {ResponseHeaders, Request, RequestState} from '@dkx/http-server';


export declare interface ResponseOptions
{
	statusCode?: number,
	statusMessage?: string,
	headers?: ResponseHeaders,
	body?: string,
	timeout?: number,
	onRequest?: (request: Request, state: RequestState) => void,
}
