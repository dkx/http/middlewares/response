import {Request, RequestState, testMiddleware} from '@dkx/http-server';
import {expect} from 'chai';
import {performance} from 'perf_hooks';
import {responseMiddleware} from '../../lib';


describe('#responseMiddleware', () => {

	it('should override status', async () => {
		const res = await testMiddleware(responseMiddleware({
			statusCode: 404,
			statusMessage: 'Not found',
		}));

		expect(res.statusCode).to.be.equal(404);
		expect(res.statusMessage).to.be.equal('Not found');
	});

	it('should set only status message', async () => {
		const res = await testMiddleware(responseMiddleware({
			statusMessage: 'OK',
		}));

		expect(res.statusCode).to.be.equal(200);
		expect(res.statusMessage).to.be.equal('OK');
	});

	it('should set response headers', async () => {
		const res = await testMiddleware(responseMiddleware({
			headers: {
				'X-Test': 'lorem ipsum',
			},
		}));

		expect(res.headers).to.be.eql({
			'x-test': 'lorem ipsum',
		});
	});

	it('should set response body', async () => {
		const body: Array<string> = [];

		await testMiddleware(responseMiddleware({
			body: 'hello world',
		}), {
			onBodyWrite: (chunk) => body.push(chunk.toString()),
		});

		expect(body).to.be.eql(['hello world']);
	});

	it('should respond after timeout', async () => {
		const start = performance.now();

		await testMiddleware(responseMiddleware({
			timeout: 500,
		}));

		const end = performance.now();
		const elapsed = Math.round((end - start) * 1000) / 1000;

		expect(elapsed).to.be.greaterThan(500);
	});

	it('should listen for onRequest event', async () => {
		let request: Request;
		let state: RequestState;

		await testMiddleware(responseMiddleware({
			onRequest: (req, reqState) => {
				request = req;
				state = reqState;
			},
		}), {
			state: {
				message: 'hello world',
			},
		});

		expect(request).to.be.an.instanceOf(Request);
		expect(state).to.be.eql({
			message: 'hello world',
		});
	});

});
