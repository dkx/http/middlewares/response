# DKX/Http/Middleware/Response

Middleware for returning custom response for [@dkx/http-server](https://gitlab.com/dkx/http/server). Great for testing.

## Installation

```bash
$ npm install --save @dkx/http-middleware-response
```

or with yarn

```bash
$ yarn add @dkx/http-middleware-response
```

## Usage

```js
const {Server} = require('@dkx/http-server');
const {responseMiddleware} = require('@dkx/http-middleware-response');

const app = new Server;
const responseOptions = {
	statusCode: 200,
	statusMessage: 'OK',
	headers: {
		'X-Custom-header': 'lorem ipsum'
	},
	body: 'hello world',
	timeout: 500,
	onRequest: function(request) {
		console.log(request);
	}
};

app.use(responseMiddleware(responseOptions));
```
